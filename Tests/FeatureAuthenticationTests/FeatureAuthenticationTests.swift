import XCTest
@testable import FeatureAuthentication

final class FeatureAuthenticationTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(FeatureAuthentication().text, "Hello, World!")
    }
}
