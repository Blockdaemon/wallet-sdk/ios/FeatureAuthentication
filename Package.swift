// swift-tools-version: 5.8
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "FeatureAuthentication",
    platforms: [
        .iOS(.v16),
        .macOS(.v13)
    ],
    products: [
        .library(
            name: "FeatureAuthentication",
            targets: ["FeatureAuthenticationData", "FeatureAuthenticationDomain"]
        ),
        .library(
            name: "FeatureAuthenticationData",
            targets: ["FeatureAuthenticationData"]
        ),
        .library(
            name: "FeatureAuthenticationDomain",
            targets: ["FeatureAuthenticationDomain"]
        )
    ],
    dependencies: [
        .package(
            url: "https://github.com/Liftric/DIKit.git",
            exact: "1.6.1"
        ),
        .package(
            url: "https://gitlab.com/Blockdaemon/wallet-sdk/ios/NetworkKit",
            exact: "1.0.0"
        ),
        .package(
            url: "https://gitlab.com/Blockdaemon/wallet-sdk/ios/Errors",
            exact: "1.0.0"
        ),
        .package(
            url: "https://gitlab.com/Blockdaemon/wallet-sdk/ios/Extensions",
            exact: "1.0.0"
        ),
        .package(
            url: "https://gitlab.com/Blockdaemon/wallet-sdk/ios/ToolKit",
            exact: "1.0.0"
        )
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "FeatureAuthenticationDomain",
            dependencies: [
                "NetworkKit",
                "Extensions",
                "Errors",
                "ToolKit"
            ]
        ),
        .target(
            name: "FeatureAuthenticationData",
            dependencies: [
                .target(name: "FeatureAuthenticationDomain"),
                .product(name: "DIKit", package: "DIKit"),
                "NetworkKit",
                "Extensions",
                "Errors"
            ]
        )
    ]
)
