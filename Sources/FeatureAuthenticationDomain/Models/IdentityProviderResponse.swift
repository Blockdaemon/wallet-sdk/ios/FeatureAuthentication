// Copyright © Blockdaemon All rights reserved.

public struct Passkey {
    public let attestationObject: String
    public let clientDataJSON: String
}

public enum IdentityProviderResult {
    
    public struct NewAccount {

        public let identifier: String
        public let raw: String
        public let passkey: Passkey
    }
        
    public struct AuthenticatedAccount {
        
        public let credentialId: String
        public let signature: String
        public let identifier: String
        public let passkey: Passkey
    }
    
    case newAccount(NewAccount)
    case authenticated(AuthenticatedAccount)
    case failure(IdentityProviderError)
}
