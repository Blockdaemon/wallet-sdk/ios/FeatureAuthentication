// Copyright © Blockdaemon All rights reserved.

import Foundation

public struct WalletCredentials: Equatable, Codable {
    
    /// Returns the stored session token
    public var sessionToken: String
    
    public static let empty: WalletCredentials = .init(
        sessionToken: ""
    )
}
