// Copyright © Blockdaemon All rights reserved.

import Foundation

public enum Credentials {
    
    public var challenge: String {
        switch self {
        case .newAccount(let credentials):
            return credentials.challenge
        case .authenticated(let credentials):
            return credentials.challenge
        }
    }
    
    case newAccount(RegistrationCredentials)
    case authenticated(AuthenticationCredentials)
    
    // MARK: - New Account
    
    /// Maps to `publicKey` in API response
    public struct RegistrationCredentials: Decodable {
        public let challenge: String
        public let user: AssociatedUser
        public let attestation: Attestation
        public let authenticatorSelection: AuthenticatorSelection
        public let pubKeyCredParams: [PubKeyCredParam]
        public let rp: RelyingParty
        public let timeout: Int
    }

    /// Maps to `user` in API response
    public struct AssociatedUser: Decodable {
        public let id: String
        public let name: String
        public let displayName: String
    }

    /// Maps to `rp` in API response
    public struct RelyingParty: Decodable {
        public let name: String
        public let id: String
    }

    public struct AuthenticatorSelection: Decodable {
        public let authenticatorAttachment: String
        public let requireResidentKey: Bool
        public let residentKey: String
        public let userVerification: String
    }

    public enum Attestation: String, Decodable {
        case none
        case indirect
        case direct
    }

    public struct PubKeyCredParam: Decodable {
        public let type: String
        public let alg: Int
    }
    
    // MARK: - AuthenticationCredentials
    
    public struct AuthenticationCredentials: Decodable {
        public let challenge: String
        public let timeout: Int
        public let rpId: String
        public let allowCredentials: [Credential]
        public let userVerification: String
        public let extensions: [String: String]?

        public struct Credential: Decodable {
            public let type: String
            public let id: String
            public let transports: [String]?
        }
    }
}
