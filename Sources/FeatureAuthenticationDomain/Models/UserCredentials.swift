// Copyright © Blockdaemon All rights reserved.

import Foundation

public struct UserCredentials: Equatable, Codable {
    
    /// Returns the stored userId
    public var userId: String
    
    public static let empty: UserCredentials = .init(
        userId: ""
    )
}
