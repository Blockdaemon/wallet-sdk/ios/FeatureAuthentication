// Copyright © Blockdaemon All rights reserved.

import Combine
import Errors
import Foundation

public protocol BlockdaemonUserCreationRepositoryAPI {
    
    /// Creates a new userId
    func createUserIdForApplicationId(
        _ applicationId: String
    ) -> AnyPublisher<String, NetworkError>
}
