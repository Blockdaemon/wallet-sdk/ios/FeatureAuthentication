// Copyright © Blockdaemon All rights reserved.

import Combine
import Foundation

@dynamicMemberLookup
public protocol BlockdaemonUserCredentialRepositoryAPI {
    
    /// Gets the current value of the `UserCredentials` (userId)
    /// - Returns: `Publishers.First<AnyPublisher<UserCredentials, Never>>`
    func get() -> Publishers.First<AnyPublisher<UserCredentials, Never>>

    /// Streams the value of the underlying state of `UserCredentials`
    /// - Returns: `AnyPublisher<UserCredentials, Never>`
    func stream() -> AnyPublisher<UserCredentials, Never>

    /// Sets the given value on the selected keyPath and return the updated state as a stream.
    ///
    /// - Parameters:
    ///   - keyPath: A `WritableKeyPath` for the underlying variable
    ///   - value: A value to be to written for the selected keyPath
    @discardableResult
    func set<Value>(
        keyPath: WritableKeyPath<UserCredentials, Value>,
        value: Value
    ) -> Self

    /// Sets an new `UserCredentials` value
    @discardableResult
    func set(
        value: UserCredentials
    ) -> Self

    /// Returns the resulting value of a given key path.
    subscript<LocalState>(
        dynamicMember keyPath: KeyPath<UserCredentials, LocalState>
    ) -> LocalState { get }

    /// Returns the resulting publisher of a given key path.
    subscript<LocalState: Equatable>(
        dynamicMember keyPath: KeyPath<UserCredentials, LocalState>
    ) -> AnyPublisher<LocalState, Never> { get }
}
