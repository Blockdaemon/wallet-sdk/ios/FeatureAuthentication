// Copyright © Blockdaemon All rights reserved.

import Combine
import Foundation

public protocol BlockdaemonUserRepositoryAPI {

    /// Streams `Bool` indicating whether a userId has been provided
    var hasUserId: AnyPublisher<Bool, Never> { get }

    /// Streams the userId
    var userId: AnyPublisher<String?, Never> { get }

    /// Sets the userId
    func set(userId: String) -> AnyPublisher<Void, Never>

    /// Cleans the userId
    func empty() -> AnyPublisher<Void, Never>
}

extension BlockdaemonUserRepositoryAPI {

    public var hasUserId: AnyPublisher<Bool, Never> {
        userId
            .flatMap { userId -> AnyPublisher<Bool, Never> in
                guard let userId else { return .just(false) }
                return .just(!userId.isEmpty)
            }
            .eraseToAnyPublisher()
    }
}
