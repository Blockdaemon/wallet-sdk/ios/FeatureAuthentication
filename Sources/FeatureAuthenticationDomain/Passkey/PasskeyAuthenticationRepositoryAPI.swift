// Copyright © Blockdaemon All rights reserved.

import Combine
import Errors

public enum PasskeyAuthenticationRepositoryError: Error, CustomStringConvertible {
    case network(NetworkError)
    
    public var description: String {
        switch self {
        case .network(let error):
            return error.description
        }
    }
}

// TODO: Separate Registration and Authentication into separate services
public protocol PasskeyAuthenticationRepositoryAPI {
    
    /// Request passkey challenge. This might be the same as the
    /// above function but, not sure yet.
    func fetchRegistrationCredentials() -> AnyPublisher<Credentials, PasskeyAuthenticationRepositoryError>
    
    /// Registers a new user and returns passkey challenge.
    func registerUserIdAndFetchRegistrationCredentials(
        _ userId: String
    ) -> AnyPublisher<Credentials, PasskeyAuthenticationRepositoryError>
    
    /// Authenticate a new user and returns passkey challenge.
    func authenticateUserIdAndFetchRegistrationCredentials(
        _ userId: String
    ) -> AnyPublisher<Credentials, PasskeyAuthenticationRepositoryError>
    
    func doValidateCredentialsAreRegisteredForUserId(
        _ userId: String
    ) -> AnyPublisher<Bool, PasskeyAuthenticationRepositoryError>
    
    func doCompletePasskeyRegistrationRequestWithNewAccount(
        _ newAccount: IdentityProviderResult.NewAccount,
        userId: String
    ) -> AnyPublisher<Void, PasskeyAuthenticationRepositoryError>
    
    func doCompletePasskeyAuthenticationRequestWithAuthenticatedAccount(
        _ authenticatedAccount: IdentityProviderResult.AuthenticatedAccount,
        userId: String
    ) -> AnyPublisher<Void, PasskeyAuthenticationRepositoryError>
}
