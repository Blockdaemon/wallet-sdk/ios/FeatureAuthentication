// Copyright © Blockdaemon All rights reserved.

import AuthenticationServices
import Combine
import DIKit
import Errors
import NetworkKit

public protocol PasskeyAuthenticationServiceAPI {
    
    /// Request passkey challenge. This might be the same as the
    /// above function but, not sure yet.
    func fetchAndAuthorizeRegistrationCredentials() -> AnyPublisher<Credentials, PasskeyAuthenticationServiceError>
    
    /// Request passkey challenge. This might be the same as the
    /// above function but, not sure yet.
    /// - Parameters:
    ///   - userId: User ID
    func registerUserIdAndFetchRegistrationCredentials(
        _ userId: String
    ) -> AnyPublisher<Credentials, PasskeyAuthenticationServiceError>
    
    func registerOrAuthenticateUserIdAndFetchCredentials(
        _ userId: String
    ) -> AnyPublisher<Credentials, PasskeyAuthenticationServiceError>
    
    func doCompletePasskeyRegistrationRequestWithNewAccount(
        _ newAccount: IdentityProviderResult.NewAccount,
        userId: String
    ) -> AnyPublisher<Void, PasskeyAuthenticationServiceError>
    
    func doValidateCredentialsAreRegisteredForUserId(
        _ userId: String
    ) -> AnyPublisher<Bool, PasskeyAuthenticationServiceError>
    
    func doCompletePasskeyAuthenticationRequestWithAuthenticatedAccount(
        _ authenticatedAccount: IdentityProviderResult.AuthenticatedAccount,
        userId: String
    ) -> AnyPublisher<Void, PasskeyAuthenticationServiceError>
}

public enum PasskeyAuthenticationServiceError: Error, CustomStringConvertible {
    case repository(PasskeyAuthenticationRepositoryError)
    
    public var description: String {
        switch self {
        case .repository(let error):
            return error.description
        }
    }
}

public final class PasskeyAuthenticationService: PasskeyAuthenticationServiceAPI {
    
    // MARK: - Private Properties

    private var cancellables = Set<AnyCancellable>()
    private let repository: PasskeyAuthenticationRepositoryAPI
    private let blockdaemonIdentityProvider: BlockdaemonIdentityProviderAPI
    
    // MARK: - Init
    
    init(
        repository: PasskeyAuthenticationRepositoryAPI = resolve(),
        blockdaemonIdentityProvider: BlockdaemonIdentityProviderAPI = resolve()
    ) {
        self.repository = repository
        self.blockdaemonIdentityProvider = blockdaemonIdentityProvider
    }
    
    // MARK: - PasskeyAuthenticationServiceAPI
    
    public func fetchAndAuthorizeRegistrationCredentials() -> AnyPublisher<Credentials, PasskeyAuthenticationServiceError> {
        repository
            .fetchRegistrationCredentials()
            .mapError(PasskeyAuthenticationServiceError.repository)
            .eraseToAnyPublisher()
    }
    
    public func registerUserIdAndFetchRegistrationCredentials(
        _ userId: String
    ) -> AnyPublisher<Credentials, PasskeyAuthenticationServiceError> {
        repository
            .registerUserIdAndFetchRegistrationCredentials(userId)
            .mapError(PasskeyAuthenticationServiceError.repository)
            .eraseToAnyPublisher()
    }
    
    public func doValidateCredentialsAreRegisteredForUserId(
        _ userId: String
    ) -> AnyPublisher<Bool, PasskeyAuthenticationServiceError> {
        repository
            .doValidateCredentialsAreRegisteredForUserId(userId)
            .mapError(PasskeyAuthenticationServiceError.repository)
            .eraseToAnyPublisher()
    }
    
    public func registerOrAuthenticateUserIdAndFetchCredentials(
        _ userId: String
    ) -> AnyPublisher<Credentials, PasskeyAuthenticationServiceError> {
        repository
            .doValidateCredentialsAreRegisteredForUserId(userId)
            .flatMap { [repository] areCredentialsRegistered -> AnyPublisher<Credentials, PasskeyAuthenticationRepositoryError> in
                if !areCredentialsRegistered {
                    return repository.registerUserIdAndFetchRegistrationCredentials(userId)
                } else {
                    return repository.authenticateUserIdAndFetchRegistrationCredentials(userId)
                }
            }
            .mapError(PasskeyAuthenticationServiceError.repository)
            .eraseToAnyPublisher()
    }
    
    public func doCompletePasskeyRegistrationRequestWithNewAccount(
        _ newAccount: IdentityProviderResult.NewAccount,
        userId: String
    ) -> AnyPublisher<Void, PasskeyAuthenticationServiceError> {
        repository
            .doCompletePasskeyRegistrationRequestWithNewAccount(
                newAccount,
                userId: userId
            )
            .mapError(PasskeyAuthenticationServiceError.repository)
            .eraseToAnyPublisher()
    }

    public func doCompletePasskeyAuthenticationRequestWithAuthenticatedAccount(
        _ authenticatedAccount: IdentityProviderResult.AuthenticatedAccount,
        userId: String
    ) -> AnyPublisher<Void, PasskeyAuthenticationServiceError> {
        repository
            .doCompletePasskeyAuthenticationRequestWithAuthenticatedAccount(
                authenticatedAccount,
                userId: userId
            )
            .mapError(PasskeyAuthenticationServiceError.repository)
            .eraseToAnyPublisher()
    }
}

