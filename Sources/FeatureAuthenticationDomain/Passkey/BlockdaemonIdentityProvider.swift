// Copyright © Blockdaemon All rights reserved.

import AuthenticationServices
import Combine
import DIKit
import Errors
import Extensions
import NetworkKit
import ToolKit

public protocol BlockdaemonIdentityProviderAPI {
    func createOrAuthenticateAccountWithRegistrationCredentials(
        _ credentials: Credentials,
        userId: String
    ) -> AnyPublisher<IdentityProviderResult, IdentityProviderError>
}

public enum IdentityProviderError: Error, CustomStringConvertible {
    case authorization(ASAuthorizationError.Code)
    case passkey(PasskeyAuthenticationServiceError)
    case unknown(Error)
    
    public var description: String {
        switch self {
        case .authorization(let code):
            return "ASAuthorizationError: \(code.rawValue)"
        case .passkey(let passkey):
            return passkey.description
        case .unknown(let error):
            return error.localizedDescription
        }
    }
}

public class BlockdaemonIdentityProvider: NSObject, BlockdaemonIdentityProviderAPI {
    
    // MARK: - Private Properties
    
    private var anchor: ASPresentationAnchor? {
        #if os(iOS)
        return UIApplication.shared.connectedScenes
            .filter { $0.activationState == .foregroundActive }
            .map { $0 as? UIWindowScene }
            .compactMap { $0 }
            .first?.windows
            .first(where: { $0.isKeyWindow })
        #elseif os(macOS)
        return NSApplication.shared.mainWindow
        #endif
    }

    private let publisher: AnyPublisher<IdentityProviderResult, IdentityProviderError>
    private let createOrAuthenticateAccountSubject = PassthroughSubject<IdentityProviderResult, IdentityProviderError>()
    private let publicKeyCredentialsProvider: ASAuthorizationPlatformPublicKeyCredentialProvider
    
    // MARK: - Init
    
    init(blockdaemonAPI: BlockdaemonAPI = resolve()) {
        let repository: ApplicationIdRepositoryAPI = resolve()
        let applicationId = repository.applicationId
        self.publicKeyCredentialsProvider = .init(relyingPartyIdentifier: "\(applicationId)." + blockdaemonAPI.rpId)
        self.publisher = createOrAuthenticateAccountSubject
            .eraseToAnyPublisher()
    }
    
    public func createOrAuthenticateAccountWithRegistrationCredentials(
        _ credentials: Credentials,
        userId: String
    ) -> AnyPublisher<IdentityProviderResult, IdentityProviderError> {
        createOrAuthenticateAccountSubject
            .handleEvents(receiveSubscription: { [weak self] _ in
                guard let self = self else { impossible() }
                let challenge = credentials.challenge
                let data = challenge.base64URLDecoded()
                
                var authorizationRequest: ASAuthorizationRequest
                switch credentials {
                case .newAccount(let account):
                    let identifier = account.user.id.base64URLDecoded()
                    authorizationRequest = self.publicKeyCredentialsProvider.createCredentialRegistrationRequest(
                        challenge: data,
                        name: userId,
                        userID: identifier
                    )
                    
                case .authenticated(let account):
                    authorizationRequest = self.publicKeyCredentialsProvider.createCredentialAssertionRequest(
                        challenge: data
                    )
                    
                    guard let request = authorizationRequest as? ASAuthorizationPlatformPublicKeyCredentialAssertionRequest else {
                        impossible()
                    }
                    let credentials = account.allowCredentials
                    var allowedCredentials: [ASAuthorizationPlatformPublicKeyCredentialDescriptor] = []
                    for credential in credentials {
                        allowedCredentials.append(.init(credentialID: credential.id.base64URLDecoded()))
                    }
                    request.allowedCredentials = allowedCredentials
                }
                
                let authController = ASAuthorizationController(
                    authorizationRequests: [authorizationRequest]
                )
                authController.delegate = self
                authController.presentationContextProvider = self
                authController.performRequests()
                
            })
            .eraseToAnyPublisher()
    }
}

extension BlockdaemonIdentityProvider: ASAuthorizationControllerPresentationContextProviding, ASAuthorizationControllerDelegate {

    public func presentationAnchor(
        for controller: ASAuthorizationController
    ) -> ASPresentationAnchor {
        guard let anchor = anchor else { impossible("Expected an ASPresentationAnchor") }
        return anchor
    }
    
    public func authorizationController(
        controller: ASAuthorizationController,
        didCompleteWithAuthorization authorization: ASAuthorization
    ) {
        switch authorization.credential {
        case let credentialRegistration as ASAuthorizationPlatformPublicKeyCredentialRegistration:
            Logger.shared.debug("A new credential was registered: \(credentialRegistration)")
            let rawClientJSON = credentialRegistration.rawClientDataJSON.base64URLEncodedString()
            let identifier = credentialRegistration.credentialID.base64URLEncodedString()
            let attestation = credentialRegistration.rawAttestationObject?.base64URLEncodedString()
            
            Logger.shared.debug("Encoded ID: \(identifier)")
            
            createOrAuthenticateAccountSubject.send(
                .newAccount(
                    .init(
                        identifier: identifier,
                        raw: identifier,
                        passkey: .init(attestationObject: attestation ?? "", clientDataJSON: rawClientJSON)
                    )
                )
            )
        case let credentialAssertion as ASAuthorizationPlatformPublicKeyCredentialAssertion:
            let signature = credentialAssertion.signature.base64URLEncodedString()
            let identifier = credentialAssertion.credentialID.base64URLEncodedString()
            let attestation = credentialAssertion.rawAuthenticatorData.base64URLEncodedString()
            let clientDataJson = credentialAssertion.rawClientDataJSON.base64URLEncodedString()
            let id = credentialAssertion.userID.base64URLEncodedString()
            
            let account = IdentityProviderResult.AuthenticatedAccount(
                credentialId: identifier,
                signature: signature,
                identifier: id,
                passkey: .init(
                    attestationObject: attestation,
                    clientDataJSON: clientDataJson
                )
            )
            
            createOrAuthenticateAccountSubject.send(
                .authenticated(
                    account
                )
            )
        default:
            impossible("Unknown authorization type")
        }
    }
    
    public func authorizationController(
        controller: ASAuthorizationController,
        didCompleteWithError error: Error
    ) {
        Logger.shared.error("Authorization controller completed with error: \(error.localizedDescription)")
        guard let error = error as? ASAuthorizationError else {
            createOrAuthenticateAccountSubject.send(.failure(.unknown(error)))
            return
        }
        switch error.code {
        case .canceled:
            createOrAuthenticateAccountSubject.send(.failure(.authorization(.canceled)))
        case .failed:
            createOrAuthenticateAccountSubject.send(.failure(.authorization(.failed)))
        case .invalidResponse:
            createOrAuthenticateAccountSubject.send(.failure(.authorization(.invalidResponse)))
        case .notHandled:
            createOrAuthenticateAccountSubject.send(.failure(.authorization(.notHandled)))
        case .unknown:
            createOrAuthenticateAccountSubject.send(.failure(.authorization(.unknown)))
        case .notInteractive:
            createOrAuthenticateAccountSubject.send(.failure(.authorization(.notInteractive)))
        @unknown default:
            createOrAuthenticateAccountSubject.send(.failure(.unknown(error)))
        }
    }
}
