// Copyright © Blockdaemon All rights reserved.

import Combine
import DIKit
import Errors
import Extensions

public enum JWTServiceError: Error, Equatable {
    case failedToRetrieveJWTToken
}

public protocol JWTServiceAPI: AnyObject {
    /// Retrieves the JWT cached in `CredentialsRepository`
    var token: AnyPublisher<String?, Never> { get }
    
    func reset() -> AnyPublisher<Void, Never>
}

final class JWTService: JWTServiceAPI {
    
    private let repository: JWTSessionTokenRepositoryAPI
    
    init(repository: JWTSessionTokenRepositoryAPI = resolve()) {
        self.repository = repository
    }
    
    var token: AnyPublisher<String?, Never> {
        repository
            .stream()
            .eraseToAnyPublisher()
    }
    
    func reset() -> AnyPublisher<Void, Never> {
        repository
            .set(value: nil)
        return .just(())
    }
}
