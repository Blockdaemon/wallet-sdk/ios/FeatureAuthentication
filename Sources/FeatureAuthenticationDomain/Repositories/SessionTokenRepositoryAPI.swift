// Copyright © Blockdaemon All rights reserved.

import Combine

public enum SessionTokenRepositoryError: Error {
    case authnSessionTokenNotSet
}

public protocol SessionTokenRepositoryAPI: AnyObject {
    
    func stream() -> AnyPublisher<String?, Never>

    func get() -> Publishers.First<AnyPublisher<String, SessionTokenRepositoryError>>
    
    subscript<LocalState>(
        dynamicMember keyPath: KeyPath<String?, LocalState>
    ) -> LocalState? { get }

    subscript<LocalState>(
        dynamicMember keyPath: KeyPath<String?, LocalState>
    ) -> AnyPublisher<LocalState, Never> where LocalState: Equatable { get }

    @discardableResult
    func set<Value>(
        keyPath: WritableKeyPath<String?, Value>,
        value: Value
    ) -> Self

    @discardableResult
    func set(
        value: String?
    ) -> Self
}
