// Copyright © Blockdaemon All rights reserved.

import Combine
import Foundation

public protocol ApplicationIdRepositoryAPI: AnyObject {

    /// The application Id
    var applicationId: String { get }
}
