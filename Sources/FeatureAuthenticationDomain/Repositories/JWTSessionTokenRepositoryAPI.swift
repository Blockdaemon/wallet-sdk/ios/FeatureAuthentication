// Copyright © Blockdaemon All rights reserved.

import Combine
import Foundation

public enum JWTUserSesstionTokenRepositoryError: Error {
    case jwtSessionTokenNotSet
}

public protocol JWTSessionTokenRepositoryAPI: AnyObject {
    func stream() -> AnyPublisher<String?, Never>
    
    func get() -> Publishers.First<AnyPublisher<String, JWTUserSesstionTokenRepositoryError>>
    
    subscript<LocalState>(
        dynamicMember keyPath: KeyPath<String?, LocalState>
    ) -> LocalState? { get }

    subscript<LocalState>(
        dynamicMember keyPath: KeyPath<String?, LocalState>
    ) -> AnyPublisher<LocalState, Never> where LocalState: Equatable { get }

    @discardableResult
    func set<Value>(
        keyPath: WritableKeyPath<String?, Value>,
        value: Value
    ) -> Self

    @discardableResult
    func set(
        value: String?
    ) -> Self
}
