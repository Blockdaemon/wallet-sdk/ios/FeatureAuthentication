// Copyright © Blockdaemon All rights reserved.

import Combine
import Foundation

@dynamicMemberLookup
public protocol WalletCredentialsRepositoryAPI {
    
    /// Gets the current value of the `WalletCredentials`
    /// - Returns: `Publishers.First<AnyPublisher<WalletCredentials, Never>>`
    func get() -> Publishers.First<AnyPublisher<WalletCredentials, Never>>

    /// Streams the value of the underlying state of `WalletCredentials`
    /// - Returns: `AnyPublisher<WalletCredentials, Never>`
    func stream() -> AnyPublisher<WalletCredentials, Never>

    /// Sets the given value on the selected keyPath and return the updated state as a stream.
    ///
    /// - Parameters:
    ///   - keyPath: A `WritableKeyPath` for the underlying variable
    ///   - value: A value to be to written for the selected keyPath
    @discardableResult
    func set<Value>(
        keyPath: WritableKeyPath<WalletCredentials, Value>,
        value: Value
    ) -> Self

    /// Sets an new `WalletCredentials` value
    ///
    /// - Parameter value: A `WalletCredentials` to be set.
    @discardableResult
    func set(
        value: WalletCredentials
    ) -> Self

    /// Returns the resulting value of a given key path.
    subscript<LocalState>(
        dynamicMember keyPath: KeyPath<WalletCredentials, LocalState>
    ) -> LocalState { get }

    /// Returns the resulting publisher of a given key path.
    subscript<LocalState: Equatable>(
        dynamicMember keyPath: KeyPath<WalletCredentials, LocalState>
    ) -> AnyPublisher<LocalState, Never> { get }
}
