// Copyright © Blockdaemon All rights reserved.

import Combine
import DIKit
import Errors

public enum BlockdaemonUserCreationServiceError: Error, CustomStringConvertible {
    case networkError(NetworkError)
    
    public var networkError: NetworkError {
        switch self {
        case .networkError(let error):
            return error
        }
    }
    
    public var description: String {
        switch self {
        case .networkError(let error):
            return error.description
        }
    }
}

public protocol BlockdaemonUserCreationServiceAPI {
    
    /// Creates a new userId and sets it in the `BlockdaemonUserRepositoryAPI`.
    /// Normally the userId is supplied by the client.
    func createAndSetUserIdForApplicationId(
        _ applicationId: String
    ) -> AnyPublisher<String, BlockdaemonUserCreationServiceError>
}


final class BlockdaemonUserCreationService: BlockdaemonUserCreationServiceAPI {

    // MARK: - Injected

    private let repository: BlockdaemonUserCreationRepositoryAPI
    private let userRepository: BlockdaemonUserRepositoryAPI

    init(
        repository: BlockdaemonUserCreationRepositoryAPI = resolve(),
        userRepository: BlockdaemonUserRepositoryAPI = resolve()
    ) {
        self.repository = repository
        self.userRepository = userRepository
    }
    
    // MARK: - BlockdaemonUserCreationServiceAPI
    
    func createAndSetUserIdForApplicationId(
        _ applicationId: String
    ) -> AnyPublisher<String, BlockdaemonUserCreationServiceError> {
        repository
            .createUserIdForApplicationId(applicationId)
            .mapError(BlockdaemonUserCreationServiceError.networkError)
            .flatMap { [userRepository] userId in
                userRepository.set(userId: userId)
                    .map { _ in
                        return userId
                    }
            }
            .eraseToAnyPublisher()
    }
}
