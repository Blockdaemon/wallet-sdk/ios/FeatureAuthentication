// Copyright © Blockdaemon All rights reserved.

import Combine
import DIKit
import Errors

public enum SessionTokenServiceError: Error, Equatable {
    case networkError(NetworkError)
    case missingSessionToken
}

public protocol SessionTokenServiceAPI: AnyObject {
    var token: AnyPublisher<String?, Never> { get }
    
    func reset() -> AnyPublisher<Void, Never>
}

final class SessionTokenService: SessionTokenServiceAPI {

    // MARK: - Injected

    private let repository: SessionTokenRepositoryAPI

    // MARK: - Setup

    init(repository: SessionTokenRepositoryAPI = resolve()) {
        self.repository = repository
    }
    
    var token: AnyPublisher<String?, Never> {
        repository
            .stream()
            .eraseToAnyPublisher()
    }
    
    func reset() -> AnyPublisher<Void, Never> {
        repository
            .set(value: nil)
        return .just(())
    }
}
