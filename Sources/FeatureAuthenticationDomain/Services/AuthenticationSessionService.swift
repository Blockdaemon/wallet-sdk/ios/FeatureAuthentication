// Copyright © Blockdaemon All rights reserved.

import Combine
import DIKit
import NetworkKit
import Errors
import Extensions

public protocol AuthenticationSessionServiceAPI {
    /// Runs authentication flow if needed and passes it to the `networkResponsePublisher`
    /// - Parameter networkResponsePublisher: the closure taking a token and returning a publisher for a request
    func authenticate(
        _ networkResponsePublisher: @escaping NetworkResponsePublisher
    ) -> AnyPublisher<ServerResponse, NetworkError>
}

public typealias AuthenticationSessionServiceProvider = () -> AuthenticationSessionServiceAPI

final class AuthenticationSessionService: AuthenticationSessionServiceAPI {
    
    private let passkey: PasskeyAuthenticationServiceAPI
    private let store: JWTSessionTokenRepositoryAPI
    
    init(
        store: JWTSessionTokenRepositoryAPI = resolve(),
        passkey: PasskeyAuthenticationServiceAPI = resolve()
    ) {
        self.store = store
        self.passkey = passkey
    }
    
    func authenticate(
        _ networkResponsePublisher: @escaping NetworkResponsePublisher
    ) -> AnyPublisher<ServerResponse, NetworkError> {
        getToken()
            .flatMap { token in
                networkResponsePublisher(token)
                    .mapError(NetworkError.init)
                    .eraseToAnyPublisher()
            }
            .eraseToAnyPublisher()
    }
    
    // MARK: - Private methods

    private func getToken() -> AnyPublisher<String, NetworkError> {
        store.get()
            .mapError(NetworkError.init)
            .eraseToAnyPublisher()
    }
}
