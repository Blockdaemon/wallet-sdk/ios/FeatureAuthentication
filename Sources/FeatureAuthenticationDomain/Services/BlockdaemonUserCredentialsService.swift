// Copyright © Blockdaemon All rights reserved.

import Combine
import DIKit
import Errors

public enum BlockdaemonUserCredentialsServiceError: Error, Equatable {
    case networkError(NetworkError)
    case missingBlockdaemonUserId
}

public protocol BlockdaemonUserCredentialsServiceAPI: AnyObject {
    func set(userId: String)
}

final class BlockdaemonUserCredentialsService: BlockdaemonUserCredentialsServiceAPI {

    // MARK: - Injected

    private let repository: BlockdaemonUserCredentialRepositoryAPI

    init(repository: BlockdaemonUserCredentialRepositoryAPI = resolve()) {
        self.repository = repository
    }
    
    // MARK: - BlockdaemonUserCredentialsServiceAPI
    
    func set(userId: String) {
        repository.set(keyPath: \.userId, value: userId)
    }
}
