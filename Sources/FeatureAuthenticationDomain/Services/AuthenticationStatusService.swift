// Copyright © Blockdaemon All rights reserved.

import Combine
import DIKit
import Errors

public protocol AuthenticationStatusServiceAPI {
    var isAuthenticated: AnyPublisher<Bool, Error> { get }
}

final class AuthenticationStatusService: AuthenticationStatusServiceAPI {

    var isAuthenticated: AnyPublisher<Bool, Error> {
        jwtService
            .token
            .replaceNil(with: "")
            .map { !$0.isEmpty }
            .replaceError(with: false)
            .eraseError()
    }
    
    private let jwtService: JWTServiceAPI
    
    init(jwtService: JWTServiceAPI = resolve()) {
        self.jwtService = jwtService
    }
}
