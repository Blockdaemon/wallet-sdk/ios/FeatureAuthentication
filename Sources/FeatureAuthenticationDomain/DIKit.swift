// Copyright © Blockdaemon All rights reserved.

import DIKit

extension DependencyContainer {
    
    // MARK: - FeatureAuthenticationData
    
    public static var featureAuthenticationDomain = module {
        
        factory { () -> AuthenticationSessionServiceProvider in
            { () -> AuthenticationSessionServiceAPI in
                DIKit.resolve()
            }
        }
        
        factory { AuthenticationSessionService() as AuthenticationSessionServiceAPI }
        
        factory { JWTService() as JWTServiceAPI }
        
        factory { SessionTokenService() as SessionTokenServiceAPI }
        
        factory { PasskeyAuthenticationService() as PasskeyAuthenticationServiceAPI }
        
        factory { BlockdaemonIdentityProvider() as BlockdaemonIdentityProviderAPI }
        
        factory { BlockdaemonUserCredentialsService() as BlockdaemonUserCredentialsServiceAPI }
        
        factory { BlockdaemonUserCreationService() as BlockdaemonUserCreationServiceAPI }
        
        factory { AuthenticationStatusService() as AuthenticationStatusServiceAPI }
    }
}
