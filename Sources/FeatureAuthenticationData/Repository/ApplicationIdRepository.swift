// Copyright © Blockdaemon All rights reserved.

import Foundation
import FeatureAuthenticationDomain

final class ApplicationIdRepository: ApplicationIdRepositoryAPI {
    
    var applicationId: String {
        guard let path = Bundle.main.path(forResource: "Info", ofType: "plist") else { fatalError("Expected an Info.plist") }
        guard let dict = NSDictionary(contentsOfFile: path) as? [String: AnyObject] else { fatalError() }
        guard let applicationId = dict["WALLET_APPLICATION_ID"] as? String else { fatalError("Expected WALLET_APPLICATION_ID") }
        return applicationId
    }
}
