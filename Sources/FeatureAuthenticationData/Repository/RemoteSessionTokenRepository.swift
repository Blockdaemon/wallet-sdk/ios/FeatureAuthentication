// Copyright © Blockdaemon All rights reserved.

import Combine
import DIKit
import FeatureAuthenticationDomain

final class RemoteSessionTokenRepository: RemoteSessionTokenRepositoryAPI {

    // MARK: - Properties

    var token: AnyPublisher<String?, SessionTokenServiceError> {
        client
            .token
            .mapError(SessionTokenServiceError.networkError)
            .eraseToAnyPublisher()
    }

    private let client: SessionTokenClientAPI

    // MARK: - Setup

    init(client: SessionTokenClientAPI = resolve()) {
        self.client = client
    }
}
