// Copyright © Blockdaemon All rights reserved.

import Combine
import FeatureAuthenticationDomain

final class JWTSessionTokenRepository: JWTSessionTokenRepositoryAPI {
    
    private var state: CurrentValueSubject<String?, Never> = .init(nil)
    private var publisher: AnyPublisher<String?, Never> {
        state
            .eraseToAnyPublisher()
    }
    
    func stream() -> AnyPublisher<String?, Never> {
        publisher
            .eraseToAnyPublisher()
    }
    
    func get() -> Publishers.First<AnyPublisher<String, JWTUserSesstionTokenRepositoryError>> {
        publisher
            .tryMap { value -> String in
                guard let t = value else {
                    throw JWTUserSesstionTokenRepositoryError.jwtSessionTokenNotSet
                }
                return t
            }
            .mapError { _ -> JWTUserSesstionTokenRepositoryError in
                    .jwtSessionTokenNotSet
            }
            .eraseToAnyPublisher()
            .first()
    }

    subscript<LocalState>(
        dynamicMember keyPath: KeyPath<String?, LocalState>
    ) -> LocalState? {
        state.value?[keyPath: keyPath]
    }

    subscript<LocalState>(
        dynamicMember keyPath: KeyPath<String?, LocalState>
    ) -> AnyPublisher<LocalState, Never>
        where LocalState: Equatable
    {
        state.map(keyPath)
            .eraseToAnyPublisher()
    }

    @discardableResult
    func set<Value>(
        keyPath: WritableKeyPath<String?, Value>,
        value: Value
    ) -> Self {
        state.value[keyPath: keyPath] = value
        return self
    }

    @discardableResult
    func set(
        value: String?
    ) -> Self {
        state.send(value)
        return self
    }

}
