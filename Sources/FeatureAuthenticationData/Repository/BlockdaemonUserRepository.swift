// Copyright © Blockdaemon All rights reserved.

import Combine
import DIKit
import FeatureAuthenticationDomain
import ToolKit

final class BlockdaemonUserRepository: BlockdaemonUserRepositoryAPI {
    
    let userId: AnyPublisher<String?, Never>

    private let userCredentialsRepository: BlockdaemonUserCredentialRepositoryAPI

    init(
        userCredentialsRepository: BlockdaemonUserCredentialRepositoryAPI
    ) {
        self.userCredentialsRepository = userCredentialsRepository

        self.userId = Deferred { [userCredentialsRepository] in
            userCredentialsRepository.get()
                .map(\.userId)
                .map { id in id.isEmpty ? nil : id }
        }
        .eraseToAnyPublisher()
    }

    func set(userId: String) -> AnyPublisher<Void, Never> {
        Deferred { [userCredentialsRepository] in
            userCredentialsRepository.set(keyPath: \.userId, value: userId)
                .get()
                .mapToVoid()
        }
        .eraseToAnyPublisher()
    }

    func empty() -> AnyPublisher<Void, Never> {
        Deferred { [userCredentialsRepository] in
            userCredentialsRepository.set(keyPath: \.userId, value: "")
                .get()
                .mapToVoid()
        }
        .eraseToAnyPublisher()
    }
}
