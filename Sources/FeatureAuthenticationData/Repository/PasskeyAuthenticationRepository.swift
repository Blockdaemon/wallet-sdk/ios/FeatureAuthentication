// Copyright © Blockdaemon All rights reserved.

import Combine
import DIKit
import Extensions
import FeatureAuthenticationDomain
import ToolKit

// TODO: Separate Registration and Authentication into separate services
final class PasskeyAuthenticationRepository: PasskeyAuthenticationRepositoryAPI {
    
    private let client: PasskeyAuthenticationClientAPI
    
    init(client: PasskeyAuthenticationClientAPI = resolve()) {
        self.client = client
    }
    
    func fetchRegistrationCredentials() -> AnyPublisher<Credentials, PasskeyAuthenticationRepositoryError> {
        client
            .fetchRegistrationCredentials()
            .mapError(PasskeyAuthenticationRepositoryError.network)
            .eraseToAnyPublisher()
    }
    
    func registerUserIdAndFetchRegistrationCredentials(
        _ userId: String
    ) -> AnyPublisher<Credentials, PasskeyAuthenticationRepositoryError> {
        client
            .registerUserIdAndFetchRegistrationCredentials(userId)
            .mapError(PasskeyAuthenticationRepositoryError.network)
            .eraseToAnyPublisher()
    }
    
    func doValidateCredentialsAreRegisteredForUserId(
        _ userId: String
    ) -> AnyPublisher<Bool, PasskeyAuthenticationRepositoryError> {
        client
            .doValidateCredentialsAreRegisteredForUserId(userId)
            .mapError(PasskeyAuthenticationRepositoryError.network)
            .eraseToAnyPublisher()
    }
    
    func authenticateUserIdAndFetchRegistrationCredentials(
        _ userId: String
    ) -> AnyPublisher<Credentials, PasskeyAuthenticationRepositoryError> {
        client
            .authenticateUserIdAndFetchCredentials(userId)
            .mapError(PasskeyAuthenticationRepositoryError.network)
            .eraseToAnyPublisher()
    }
    
    func doCompletePasskeyRegistrationRequestWithNewAccount(
        _ newAccount: IdentityProviderResult.NewAccount,
        userId: String
    ) -> AnyPublisher<Void, PasskeyAuthenticationRepositoryError> {
        let request = PasskeyRegistrationRequest(
            id: newAccount.identifier,
            rawId: newAccount.raw,
            response: .init(
                attestationObject: newAccount.passkey.attestationObject,
                clientDataJSON: newAccount.passkey.clientDataJSON
            )
        )
        return client
            .doCompletePasskeyRegistrationRequest(request, userId: userId)
            .mapError(PasskeyAuthenticationRepositoryError.network)
            .eraseToAnyPublisher()
    }
    
    func doCompletePasskeyAuthenticationRequestWithAuthenticatedAccount(
        _ authenticatedAccount: IdentityProviderResult.AuthenticatedAccount,
        userId: String
    ) -> AnyPublisher<Void, PasskeyAuthenticationRepositoryError> {
        let request = PasskeyAuthenticationRequest(
            id: authenticatedAccount.credentialId,
            rawId: authenticatedAccount.credentialId,
            response: .init(
                authenticatorData: authenticatedAccount.passkey.attestationObject,
                signature: authenticatedAccount.signature,
                clientDataJSON: authenticatedAccount.passkey.clientDataJSON,
                userHandle: authenticatedAccount.identifier
            ),
            authenticatorAttachment: authenticatedAccount.passkey.attestationObject
        )
        
        return client
            .doCompletePasskeyAuthenticationRequest(
                request,
                userId: userId
            )
            .mapError(PasskeyAuthenticationRepositoryError.network)
            .eraseToAnyPublisher()
    }
}
