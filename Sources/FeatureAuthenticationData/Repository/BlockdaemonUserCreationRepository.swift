// Copyright © Blockdaemon All rights reserved.

import Combine
import Errors
import FeatureAuthenticationDomain

final class BlockdaemonUserCreationRepository: BlockdaemonUserCreationRepositoryAPI {
    
    private let client: BlockdaemonUserCreationClientAPI
    
    init(client: BlockdaemonUserCreationClientAPI) {
        self.client = client
    }
    
    func createUserIdForApplicationId(
        _ applicationId: String
    ) -> AnyPublisher<String, NetworkError> {
        client
            .createUserIdForApplicationId(applicationId)
    }
}
