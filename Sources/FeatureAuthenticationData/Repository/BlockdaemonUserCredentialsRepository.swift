// Copyright © Blockdaemon All rights reserved.

import Combine
import FeatureAuthenticationDomain

final class BlockdaemonUserCredentialRepository: BlockdaemonUserCredentialRepositoryAPI {
    
    private var state: CurrentValueSubject<UserCredentials, Never>
    private let publisher: AnyPublisher<UserCredentials, Never>

    init(initialState: UserCredentials) {
        self.state = CurrentValueSubject<UserCredentials, Never>(initialState)
        self.publisher = state
            .eraseToAnyPublisher()
    }

    /// Gets the current value of the `UserCredentials`
    /// - Returns: `Publisher.First<UserCredentials, Never>`
    func get() -> Publishers.First<AnyPublisher<UserCredentials, Never>> {
        publisher.first()
    }

    /// Streams the value of the underlying state of `UserCredentials`
    /// - Returns: `AnyPublisher<UserCredentials, Never>`
    func stream() -> AnyPublisher<UserCredentials, Never> {
        publisher.eraseToAnyPublisher()
    }

    /// Returns the resulting value of a given key path.
    subscript<LocalState>(
        dynamicMember keyPath: KeyPath<UserCredentials, LocalState>
    ) -> LocalState {
        state.value[keyPath: keyPath]
    }

    /// Returns the resulting publisher of a given key path.
    subscript<LocalState>(
        dynamicMember keyPath: KeyPath<UserCredentials, LocalState>
    ) -> AnyPublisher<LocalState, Never>
        where LocalState: Equatable
    {
        state.map(keyPath)
            .eraseToAnyPublisher()
    }

    /// Sets an new `UserCredentials` value
    /// - Parameter value: A `UserCredentials` to be set.
    @discardableResult
    func set<Value>(
        keyPath: WritableKeyPath<UserCredentials, Value>,
        value: Value
    ) -> Self {
        state.value[keyPath: keyPath] = value
        return self
    }

    /// Sets an new `UserCredentials` value.
    ///
    /// - Parameter value: A `UserCredentials` to be set.
    @discardableResult
    func set(
        value: UserCredentials
    ) -> Self {
        state.send(value)
        return self
    }

}
