// Copyright © Blockdaemon All rights reserved.

import Combine
import DIKit
import FeatureAuthenticationDomain
import NetworkKit
import Extensions
import Errors

final class Authenticator: AuthenticatorAPI {
    
    private var authenticationSessionService: AuthenticationSessionServiceAPI {
        sessionServiceProvider()
    }
    
    private let sessionServiceProvider: AuthenticationSessionServiceProvider
    
    // MARK: - Setup
    
    init(sessionServiceProvider: @escaping AuthenticationSessionServiceProvider = resolve()) {
        self.sessionServiceProvider = sessionServiceProvider
    }
    
    // MARK: - AuthenticatorAPI
    
    func authenticate(
        _ responseProvider: @escaping NetworkResponsePublisher
    ) -> AnyPublisher<ServerResponse, NetworkError> {
        authenticationSessionService.authenticate(responseProvider)
    }
}

final class AuthnCeremonySessionTokenRepository: SessionTokenRepositoryAPI {
    
    private var state: CurrentValueSubject<String?, Never> = .init(nil)
    private var publisher: AnyPublisher<String?, Never> {
        state
            .eraseToAnyPublisher()
    }
    
    func stream() -> AnyPublisher<String?, Never> {
        publisher
            .eraseToAnyPublisher()
    }
    
    func get() -> Publishers.First<AnyPublisher<String, SessionTokenRepositoryError>> {
        publisher
            .tryMap { value -> String in
                guard let t = value else {
                    throw SessionTokenRepositoryError.authnSessionTokenNotSet
                }
                return t
            }
            .mapError { _ -> SessionTokenRepositoryError in
                    .authnSessionTokenNotSet
            }
            .eraseToAnyPublisher()
            .first()
    }

    subscript<LocalState>(
        dynamicMember keyPath: KeyPath<String?, LocalState>
    ) -> LocalState? {
        state.value?[keyPath: keyPath]
    }

    subscript<LocalState>(
        dynamicMember keyPath: KeyPath<String?, LocalState>
    ) -> AnyPublisher<LocalState, Never>
        where LocalState: Equatable
    {
        state.map(keyPath)
            .eraseToAnyPublisher()
    }

    @discardableResult
    func set<Value>(
        keyPath: WritableKeyPath<String?, Value>,
        value: Value
    ) -> Self {
        state.value[keyPath: keyPath] = value
        return self
    }

    @discardableResult
    func set(
        value: String?
    ) -> Self {
        state.send(value)
        return self
    }

}

