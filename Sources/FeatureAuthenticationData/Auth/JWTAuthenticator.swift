// Copyright © Blockdaemon All rights reserved.

import Combine
import DIKit
import FeatureAuthenticationDomain
import NetworkKit
import Extensions
import Errors

final class JWTAuthenticator: AuthenticatorAPI {
    
    private var authenticationSessionService: AuthenticationSessionServiceAPI {
        sessionServiceProvider()
    }
    
    private let sessionServiceProvider: AuthenticationSessionServiceProvider
    
    // MARK: - Setup
    
    init(sessionServiceProvider: @escaping AuthenticationSessionServiceProvider = resolve()) {
        self.sessionServiceProvider = sessionServiceProvider
    }
    
    // MARK: - AuthenticatorAPI
    
    func authenticate(
        _ responseProvider: @escaping NetworkResponsePublisher
    ) -> AnyPublisher<ServerResponse, NetworkError> {
        authenticationSessionService.authenticate(responseProvider)
    }
}
