// Copyright © Blockdaemon All rights reserved.

struct PasskeyRegistrationRequest: Encodable {
    struct EmptyJSONObject: Codable {}
    
    let id: String
    let rawId: String
    let authenticatorAttachment: String
    let response: Response
    let type: String
    let transports: [String]
    let clientExtensionResults: EmptyJSONObject
    
    struct Response: Encodable {
        let attestationObject: String
        let clientDataJSON: String
        let transports: [String]
        
        init(attestationObject: String, clientDataJSON: String, transports: [String] = []) {
            self.attestationObject = attestationObject
            self.clientDataJSON = clientDataJSON
            self.transports = transports
        }
    }
    
    init(
        id: String,
        rawId: String,
        authenticatorAttachment: String = "cross-platform",
        response: Response,
        type: String = "public-key",
        transports: [String] = [],
        clientExtensionResults: EmptyJSONObject = .init()
    ) {
        self.id = id
        self.rawId = rawId
        self.authenticatorAttachment = authenticatorAttachment
        self.response = response
        self.type = type
        self.transports = transports
        self.clientExtensionResults = clientExtensionResults
    }
}
