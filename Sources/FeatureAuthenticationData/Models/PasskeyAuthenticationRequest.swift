// Copyright © Blockdaemon All rights reserved.

struct PasskeyAuthenticationRequest: Encodable {
    let id: String
    let rawId: String
    let type: String
    let response: Response
    let authenticatorAttachment: String

    struct Response: Encodable {
        let authenticatorData: String
        let signature: String
        let clientDataJSON: String
        let userHandle: String
    }
    
    init(
        id: String,
        rawId: String,
        type: String = "public-key",
        response: Response,
        authenticatorAttachment: String
    ) {
        self.id = id
        self.rawId = rawId
        self.type = type
        self.response = response
        self.authenticatorAttachment = authenticatorAttachment
    }
}
