// Copyright © Blockdaemon All rights reserved.

import DIKit
import FeatureAuthenticationDomain
import NetworkKit

extension DependencyContainer {
    
    // MARK: - FeatureAuthenticationData
    
    public static var featureAuthenticationData = module {
        
        factory { APIClient() as PasskeyAuthenticationClientAPI }
        
        factory { APIClient() as BlockdaemonUserCreationClientAPI }
        
        factory { PasskeyAuthenticationRepository() as PasskeyAuthenticationRepositoryAPI }
        
        factory { RemoteSessionTokenRepository() as RemoteSessionTokenRepositoryAPI }
        
        single { AuthnCeremonySessionTokenRepository() as SessionTokenRepositoryAPI }
        
        single { JWTAuthenticator() as AuthenticatorAPI }
        
        single { JWTSessionTokenRepository() as JWTSessionTokenRepositoryAPI }
        
        single { ApplicationIdRepository() as ApplicationIdRepositoryAPI }
        
        single { () -> BlockdaemonUserCredentialRepositoryAPI in
            BlockdaemonUserCredentialRepository(initialState: .empty) as BlockdaemonUserCredentialRepositoryAPI
        }
        
        factory { () -> BlockdaemonUserCreationRepositoryAPI in
            BlockdaemonUserCreationRepository(client: DIKit.resolve())
        }
        
        factory { () -> BlockdaemonUserRepositoryAPI in
            BlockdaemonUserRepository(
                userCredentialsRepository: DIKit.resolve()
            )
        }
    }
}
