// Copyright © Blockdaemon All rights reserved.

import Combine
import Errors
import FeatureAuthenticationDomain

protocol BlockdaemonUserCreationClientAPI {
    func createUserIdForApplicationId(
        _ applicationId: String
    ) -> AnyPublisher<String, NetworkError>
}
