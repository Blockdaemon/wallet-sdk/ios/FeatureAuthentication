// Copyright © Blockdaemon All rights reserved.

import Combine
import DIKit
import Errors
import Extensions
import FeatureAuthenticationDomain
import Foundation
import NetworkKit
import ToolKit

typealias FeatureAuthenticationClientAPI = PasskeyAuthenticationClientAPI &
                                           BlockdaemonUserCreationClientAPI

final class APIClient: FeatureAuthenticationClientAPI {
    
    private enum Path {

        static let registrationOptions = ["webauthn-credentials", "register"]
        
        static func createUserForApplicationId(
            _ applicationId: String
        ) -> [String] {
            ["waas", "v1", "applications", applicationId, "application-users"]
        }
        
        static func checkIfCredentialsExistForUserId(
            _ userId: String
        ) -> [String] {
            ["waas", "v1", "application-users", userId, "webauthn-credentials", "existence"]
        }
        
        static func authenticationOptionsWithUserId(
            _ userId: String
        ) -> [String] {
            ["waas", "v1", "application-users", userId, "webauthn-credentials", "authenticate"]
        }
        
        static func registrationOptionsWithUserId(
            _ userId: String
        ) -> [String] {
            ["waas", "v1", "application-users", userId, "webauthn-credentials", "register"]
        }
    }

    private enum Parameter {
        static let userId = "userId"
    }
    
    private var authSessionToken: AnyPublisher<String, Error> {
        authnSessionTokenRepository
            .get()
            .eraseError()
    }
    
    private let networkAdapter: NetworkAdapterAPI
    private let requestBuilder: RequestBuilder
    private let jwtSessionTokenRepository: JWTSessionTokenRepositoryAPI = resolve()
    private let authnSessionTokenRepository: SessionTokenRepositoryAPI = resolve()
    
    // MARK: - Init

    init(
        requestBuilder: RequestBuilder = resolve(),
        networkAdapter: NetworkAdapterAPI = resolve()
    ) {
        self.requestBuilder = requestBuilder
        self.networkAdapter = networkAdapter
    }
    
    // MARK: - PasskeyAuthenticationClientAPI
    
    func doValidateCredentialsAreRegisteredForUserId(
        _ userId: String
    ) -> AnyPublisher<Bool, NetworkError> {
        
        struct Response: Decodable {
            let exist: Bool
        }
        
        let request = requestBuilder.get(
            path: Path.checkIfCredentialsExistForUserId(userId),
            authenticated: false
        )!
        
        return networkAdapter
            .perform(request: request, responseType: Response.self)
            .map(\.exist)
            .eraseToAnyPublisher()
    }
    
    func fetchRegistrationCredentials() -> AnyPublisher<Credentials, NetworkError> {
        let request = requestBuilder.get(
            path: Path.registrationOptions,
            authenticated: false
        )!
        
        return networkAdapter
            .perform(request: request, responseType: Credentials.RegistrationCredentials.self)
            .map { .newAccount($0) }
            .eraseToAnyPublisher()
    }
    
    func authenticateUserIdAndFetchCredentials(
        _ userId: String
    ) -> AnyPublisher<Credentials, NetworkError> {
        
        struct Response: Decodable {
            let publicKey: Credentials.AuthenticationCredentials
        }
        
        let request = requestBuilder.get(
            path: Path.authenticationOptionsWithUserId(userId),
            headers: [HttpHeaderField.contentType: HttpHeaderValue.json],
            authenticated: false
        )!
        
        return networkAdapter
            .perform(request: request, responseType: RawServerResponse.self)
            .flatMap { [authnSessionTokenRepository] response -> AnyPublisher<Credentials, NetworkError> in
                guard let data = response.data.data(using: .utf8) else {
                    return Fail(error: NetworkError.unknown).eraseToAnyPublisher()
                }
                guard let headers = response.allHeaderFields, let token = headers["wauthn-ceremony-session"] else {
                    return Fail(error: NetworkError.emptyHeader).eraseToAnyPublisher()
                }
                
                authnSessionTokenRepository.set(value: token)
                return Just(data)
                    .decode(type: Response.self, decoder: JSONDecoder())
                    .map(\.publicKey)
                    .map { .authenticated($0) }
                    .mapError { _ in NetworkError.unknown }
                    .eraseToAnyPublisher()
            }
            .eraseToAnyPublisher()
    }
    
    func registerUserIdAndFetchRegistrationCredentials(
        _ userId: String
    ) -> AnyPublisher<Credentials, NetworkError> {
        
        struct Response: Decodable {
            let publicKey: Credentials.RegistrationCredentials
        }
        
        let request = requestBuilder.get(
            path: Path.registrationOptionsWithUserId(userId),
            headers: [
                HttpHeaderField.contentType: HttpHeaderValue.json
            ],
            authenticated: false
        )!
        return networkAdapter
            .perform(request: request, responseType: RawServerResponse.self)
            .flatMap { [authnSessionTokenRepository] response -> AnyPublisher<Credentials, NetworkError> in
                guard let data = response.data.data(using: .utf8) else {
                    return Fail(error: NetworkError.unknown).eraseToAnyPublisher()
                }
                guard let headers = response.allHeaderFields, let token = headers["wauthn-ceremony-session"] else {
                    return Fail(error: NetworkError.emptyHeader).eraseToAnyPublisher()
                }
                
                authnSessionTokenRepository.set(value: token)
                return Just(data)
                    .decode(type: Response.self, decoder: JSONDecoder())
                    .map(\.publicKey)
                    .map { .newAccount($0) }
                    .mapError { error in
                        Logger.shared.error("\(error)")
                        return NetworkError.unknown
                    }
                    .eraseToAnyPublisher()
            }
            .eraseToAnyPublisher()
    }
    
    
    func doCompletePasskeyRegistrationRequest(
        _ passkeyRegistrationRequest: PasskeyRegistrationRequest,
        userId: String
    ) -> AnyPublisher<Void, NetworkError> {
        struct Response: Decodable {
            let code: Int
            let message: String
        }
        
        return authSessionToken
            .replaceError(with: NetworkError.emptyHeader)
            .flatMap { [networkAdapter, requestBuilder] authnSessionToken -> AnyPublisher<RawServerResponse, NetworkError> in
                let request = requestBuilder.post(
                    path: Path.registrationOptionsWithUserId(userId),
                    body: try? JSONEncoder().encode(passkeyRegistrationRequest),
                    headers: [
                        HttpHeaderField.contentType: HttpHeaderValue.json,
                        HttpHeaderField.webauthnToken: authnSessionToken
                    ],
                    authenticated: false
                )!
                
                return networkAdapter.perform(request: request, responseType: RawServerResponse.self)
            }
            .flatMap { [jwtSessionTokenRepository] response -> AnyPublisher<Void, NetworkError> in
                guard let data = response.data.data(using: .utf8) else {
                    return Fail(error: NetworkError.unknown).eraseToAnyPublisher()
                }
                guard let headers = response.allHeaderFields, let token = headers["user-session"] else {
                    return Fail(error: NetworkError.emptyHeader).eraseToAnyPublisher()
                }
                
                jwtSessionTokenRepository.set(value: token)
                return Just(data)
                    .decode(type: Response.self, decoder: JSONDecoder())
                    .mapToVoid()
                    .mapError { error -> NetworkError in
                        Logger.shared.error("\(error)")
                        return NetworkError.unknown
                    }
                    .eraseToAnyPublisher()
            }
            .eraseToAnyPublisher()
    }
    
    func doCompletePasskeyAuthenticationRequest(
        _ passkeyAuthenticationRequest: PasskeyAuthenticationRequest,
        userId: String
    ) -> AnyPublisher<Void, NetworkError> {
        struct Response: Decodable {
            let code: Int
            let message: String
        }
        
        return authSessionToken
            .replaceError(with: NetworkError.emptyHeader)
            .flatMap { [networkAdapter, requestBuilder] authnSessionToken -> AnyPublisher<RawServerResponse, NetworkError> in
                let request = requestBuilder.post(
                    path: Path.authenticationOptionsWithUserId(userId),
                    body: try? JSONEncoder().encode(passkeyAuthenticationRequest),
                    headers: [
                        HttpHeaderField.contentType: HttpHeaderValue.json,
                        HttpHeaderField.webauthnToken: authnSessionToken
                    ],
                    authenticated: false
                )!
                
                return networkAdapter.perform(request: request, responseType: RawServerResponse.self)
            }
            .flatMap { [jwtSessionTokenRepository] response -> AnyPublisher<Void, NetworkError> in
                guard let data = response.data.data(using: .utf8) else {
                    return Fail(error: NetworkError.unknown).eraseToAnyPublisher()
                }
                guard let headers = response.allHeaderFields, let token = headers["user-session"] else {
                    return Fail(error: NetworkError.emptyHeader).eraseToAnyPublisher()
                }
                
                jwtSessionTokenRepository.set(value: token)
                return Just(data)
                    .decode(type: Response.self, decoder: JSONDecoder())
                    .mapToVoid()
                    .mapError { error -> NetworkError in
                        Logger.shared.error("\(error)")
                        return NetworkError.unknown
                    }
                    .eraseToAnyPublisher()
            }
            .eraseToAnyPublisher()
    }
    
    // MARK: - BlockdaemonUserCreationClient
    
    func createUserIdForApplicationId(
        _ applicationId: String
    ) -> AnyPublisher<String, NetworkError> {
        
        struct Response: Decodable {
            let userId: String
        }
        
        let request = requestBuilder.post(
            path: Path.createUserForApplicationId(applicationId),
            headers: [HttpHeaderField.accept: HttpHeaderValue.json,
                      "org-username": ""],
            authenticated: false
        )!
        
        return networkAdapter
            .perform(
                request: request,
                responseType: Response.self
            )
            .map(\.userId)
            .eraseToAnyPublisher()
    }

}
