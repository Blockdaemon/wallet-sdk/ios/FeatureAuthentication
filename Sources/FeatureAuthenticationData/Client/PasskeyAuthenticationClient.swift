// Copyright © Blockdaemon All rights reserved.

import Combine
import Errors
import FeatureAuthenticationDomain
import Foundation
import ToolKit

protocol PasskeyAuthenticationClientAPI {
    
    func doValidateCredentialsAreRegisteredForUserId(
        _ userId: String
    ) -> AnyPublisher<Bool, NetworkError>
    
    func fetchRegistrationCredentials() -> AnyPublisher<Credentials, NetworkError>
    
    func authenticateUserIdAndFetchCredentials(
        _ userId: String
    ) -> AnyPublisher<Credentials, NetworkError>
    
    func registerUserIdAndFetchRegistrationCredentials(
        _ userId: String
    ) -> AnyPublisher<Credentials, NetworkError>
    
    func doCompletePasskeyRegistrationRequest(
        _ passkeyRegistrationRequest: PasskeyRegistrationRequest,
        userId: String
    ) -> AnyPublisher<Void, NetworkError>
    
    func doCompletePasskeyAuthenticationRequest(
        _ passkeyAuthenticationRequest: PasskeyAuthenticationRequest,
        userId: String
    ) -> AnyPublisher<Void, NetworkError>
}
